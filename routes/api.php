<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('profile', 'ChildController@profile');
Route::post('revalidate', 'UserController@revalidate')->middleware('cors');
Route::post('videos', 'VideosController@create');
Route::delete('deletevid', 'VideosController@deletevid');
Route::delete('destroy/{id}', 'VideoController@destroy');
Route::delete('destroyProfile/{id}', 'ChildrenController@destroyProfile');
Route::patch('update/{id}', 'VideoController@update');
Route::patch('updateProfile/{id}', 'ChildrenController@updateProfile');
Route::get('index', 'VideoController@index');

Route::post('login', 'UserController@authenticate');
Route::get('open', 'DataController@open');
Route::get('/home', 'VideosController@index')->name('home');
Route::group(['middleware' => ['jwt.verify']], function() {
Route::get('user', 'UserController@getAuthenticatedUser');
Route::get('closed', 'DataController@closed');
    });

