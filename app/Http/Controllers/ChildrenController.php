<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Profile;

use Illuminate\Http\Request;

class ChildrenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     //this method makes an update at the table Profil
    public function updateProfile(Request $request, $id)
    {
        
        $video = Profile::find($id);
        if (!$video) {
            return response()->json(['error' => 'child not found'], 404);
        }
        $user = $request->get('user');
        $age = $request->get('age');
        $pin = $request->get('pin');
        $fullname = $request->get('fullname');
        //validationes
        if ($request->method() === 'PATCH') {
            $flag = false;
            if ($user) {
                $video->user = $user;
                $flag = true;
            }
            if ($age) {
                $video->age = $age;
                $flag = true;
            }
            if ($pin) {
                $video->pin = $pin;
                $flag = true;
            }
            if ($fullname) {
                $video->fullname = $fullname;
                $flag = true;
            }
            if ($flag) {
                //if all works it saves
                $video->save();
                return response()->json(['status'=>'ok','data'=>$video], 200);
            }
            else {
                return response()->json(['errors'=>array(['code'=>304, 'message'=>'There is no value in the parameters'])], 304);
            }
        }
        //if theres no user or fullname theres no data
        if (!$user || !$fullname) {
            return response()->json($fullname);
            return response()->json(['errors'=>array(['code'=>422, 'message'=>'theres a request without parameters'])], 422);
        }
        //return code
        $video->fill($request->all())->save();
        $video = Video::find($id);
        return response()->json(['status'=>'ok','data'=>$video], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     //this method makes a destroy into the Profile table
    public function destroyProfile($id)
    {
        $video = Profile::find($id);
        if (!$video) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'profile not found'])], 404);
        }
        $video->delete();
        return response()->json(['status'=>'ok', 'data'=>$video], 200);
    }
}
