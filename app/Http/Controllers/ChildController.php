<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Illuminate\Support\Facades\DB;

use Tymon\JWTAuth\Exceptions\JWTException;
class ChildController extends Controller
{
    public function index()
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $tours = DB::table('profile')->paginate(5);
                return view('/childview', ['tours' => $tours]);
            } 
        } catch (\Exception $e) { 

            abort(404, 'No conection');

            }
       
    }
    //method to create a profile insertion
    public function profile(Request $request)
        {
            
                $validator = Validator::make($request->all(), [
                'user' => 'required|string|max:255',
                'age' => 'required|string|max:255',
                'pin' => 'required|integer|max:255',
                'fullname' => 'required|string|max:255',

            ]);

            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }

            $video = new Profile;
            $video -> user = $request->get('user');
            $video -> age = $request->get('age');
            $video -> pin = $request->get('pin');
            $video -> fullname = $request->get('fullname');
            $video->save();
               
            

            

            return response()->json(compact('video'),201);
        }
}
