<?php

namespace App\Http\Controllers;

use App\Video;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data' => Video::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //at first create a Video instance for making the vid
        $video = Video::find($id);
        if (!$video) {
            return response()->json(['error' => 'Video not found'], 404);
        }
        $name = $request->get('name');
        $kind = $request->get('kind');
        $url = $request->get('url');
        $file = $request->get('file');

        //validates a request 'pacth' following the rest patron
        if ($request->method() === 'PATCH') {
            $flag = false;
            if ($name) {
                $video->name = $name;
                $flag = true;
            }
            if ($kind) {
                $video->kind = $kind;
                $flag = true;
            }
            if ($url) {
                $video->url = $url;
                $flag = true;
            }
            if ($file) {
                $video->file = $file;
                $flag = true;
            }
            if ($flag) {
                $video->save();
                return response()->json(['status'=>'ok','data'=>$video], 200);
            }
            else {
                //no values on the parameters
                return response()->json(['errors'=>array(['code'=>304, 'message'=>'There is no value in the video parameters'])], 304);
            }
        }
        if (!$name || !$file) {
            return response()->json($file);
            //no parameters in the request
            return response()->json(['errors'=>array(['code'=>422, 'message'=>'Request without parameters'])], 422);
        }
        $video->fill($request->all())->save();
        $video = Video::find($id);
        return response()->json(['status'=>'ok','data'=>$video], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     //method used to elminate the video 
    public function destroy($id)
    {
        $video = Video::find($id);
        if (!$video) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'Video not found'])], 404);
        }
        $video->delete();
        return response()->json(['status'=>'ok', 'data'=>$video], 200);
    }
}
