<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Authy\AuthyApi as AuthyApi;
use Illuminate\Support\Facades\Auth as Auth;



class UserController extends Controller
{
    //this is used to login the registered users
        public function authenticate(Request $request)
        {
            
            $credentials = $request->only('email', 'password');

            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
            //request the api key and the users data
            $authy_api = new AuthyApi(env('AUTHY_API_KEY'));
            $user = User::where('email', '=', $request->get('email'))->first();
            $verification = $authy_api->verifyToken($user->authid, $request->get('token'));

            if ($verification->ok()) {
                return response()->json(compact('token'));
            }
                return response()->json(['error' => 'Your authy token is invalid'], 400);

            
        }

        public function revalidate(Request $request)
        {
            
    
            //this is the two factor validation
        $credentials = $request->only('email', 'password');
        //use of the authy api library
        $authy_api = new AuthyApi(env('AUTHY_API_KEY'));
        
        if (Auth::validate($credentials)) {
            
            $user = User::where('email', '=', $request->get('email'))->first();
            $sms = $authy_api->requestSms($user->authid, array("force" => "true"));
            //return a response saying that a message to the phone was sent
            return response()->json(['Sucessful' => 'A sms was send to your phone number for the authentication'], 200);
        }
        return response()->json(['error' => 'invalid_credentials'], 400);
    
        }

        //register method
        public function register(Request $request)
        {
            //this validates the fields before introducing at the db
                $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'country' => 'sometimes',
                'birthday' => 'required|string|max:255',
                'phone_number' => 'required|string|max:255',
                'country_code' => 'required|string|max:255',
                'lastname' => 'required|string|max:255',
                'age' => 'required|integer|min:18',
            ]);

            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }


           
           //create new user for sending at db
            $user = new User;         
            $authy_api = new AuthyApi(env('AUTHY_API_KEY'));          
            $user->name = $request->get('name'); 
            $user->email = $request->get('email');         
            $user->password = Hash::make($request->get('password'));         
            $user->lastname = $request->get('lastname'); 
            $user->age = $request->get('age'); 

            //validates one field to know if it has data on the request
            if ($request->get('country')) {             	
                $user->country = $request->get('country');         
            }         
                $user->birthday = $request->get('birthday');         
                $user->phone_number = $request->get('phone_number');         
                $user->country_code = $request->get('country_code');         
                $authyUser = $authy_api->registerUser($user->email, $user->phone_number, $user->country_code); //email, cellphone, country_code   
                //if theres and uthy user      
                if ($authyUser->ok()) {       
                    //set the authy user id      
                    $user->authid = $authyUser->id();         
                }         
                else {             
                    return response()->json(['error' => 'The registration on authy api'], 400);         
                }          
                //then save the user with the authy id.
                $user->save();
            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user','token'),201);

        }

        //method that helps to find the token of the user created
        public function getAuthenticatedUser()
            {
                    try {

                            if (! $user = JWTAuth::parseToken()->authenticate()) {
                                    return response()->json(['user_not_found'], 404);
                            }

                    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                            return response()->json(['token_expired'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                            return response()->json(['token_invalid'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                            return response()->json(['token_absent'], $e->getStatusCode());

                    }

                    return response()->json(compact('user'));
            }
}
