<?php

namespace App\Http\Controllers;

use App\Video;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Illuminate\Support\Facades\DB;

use Tymon\JWTAuth\Exceptions\JWTException;

class VideosController extends Controller
{

    public function index()
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $tours = DB::table('videos')->paginate(5);
                return view('/home', ['tours' => $tours]);
            } 
        } catch (\Exception $e) { 

            abort(404, 'No conection');

            }
       
    }
    public function index1()
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $tours = DB::table('videos')->paginate(5);
                return view('/guestview', ['tours' => $tours]);
            } 
        } catch (\Exception $e) { 

            abort(404, 'No conection');

            }
       
    }
    public function create(Request $request)
        {
            
                $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'kind' => 'required|string|max:255',
                'url' => 'sometimes',
                'file' => 'sometimes',

            ]);

            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }

            $video = new Video;
            $video -> name = $request->get('name');
            $video -> kind = $request->get('kind');
            $video -> url = $request->get('url');
            $video -> file = $request->get('file');
            $video->save();
               
            

            

            return response()->json(compact('video'),201);
        }
        public function deletevid(Request $request)
        {
            
            
            $video = Video::find($id);
        if (!$video) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'Video not found'])], 404);
        }
        $video->delete();
        return response()->json(['status'=>'ok', 'data'=>$video], 200);
        }
}
