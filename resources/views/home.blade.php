@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">General Playlist</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    those are the videos that have been added
                </div>
            </div>
        </div>
    </div>
</div>


<div class="categoria">
	<div class="container">
		<table class="table table-dark table-bordered">
			<tr>
				<th class="text-center">name</th>
				<th class="text-center">kind</th>
				<th class="text-center">url</th>
				<th class="text-center">file</th>
				
				<th class="text-center">
					<a href="newtour/create" class="btn btn-succes mr-2" id="add_new">
						<i class="fas fa-plus-circle"></i>
					</a>
				</th>
			</tr>
			<!--Cargo los atributos de productos con el foreach-->
			@foreach($tours as $tour)
			<tr>
				<td class="text-center">{{$tour->name}}</td>
				<td class="text-center">{{$tour->kind}}</td>
				<td class="text-center">{{$tour->url}}</td>
                
				<!--Busco el nombre del guia con el id del producto-->
				
				<td class="text-center">
					<img src="/img/{{$tour->file}}" class="imagen" />
				</td>
				<td class="text-center">
			

                <a href="/book/{{$tour->id}}" class="btn btn-sm  btn-warning"><i class="fab fa-earlybirds"></i>edit</a>    
                <a href="/book/{{$tour->id}}" class="btn btn-sm  btn-warning"><i class="fab fa-earlybirds"></i>delete</a>   
				</td>
			</tr>
            
			@endforeach
            <a href="/book/{{$tour->id}}" class="btn btn-sm  btn-warning"><i class="fab fa-earlybirds"></i>Add new video</a>
           
		</table>
		<!--Divisor de pagina-->
		{{$tours->links() }}
	</div>
</div>
</div>
@endsection
